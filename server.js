const express = require('express');
const morgan  = require('morgan')
const path = require('path');
require('dotenv').config()

const app = express();

//app.use(morgan('combined'))


//check oracle connection
const oracledb = require('oracledb');
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
// bqmdev schema password
var password = process.env.oraclePwd 



// checkConnection asycn function
try {
  oracledb.initOracleClient({libDir: './instantclient_19_6'});
} catch (err) {
  console.error('Whoops!');
  console.error(err);
  process.exit(1);
}
// var timeout = require('connect-timeout'); //express v4

//   app.use(timeout('30s'));
//   app.use(haltOnTimedout);
  
//   function haltOnTimedout(req, res, next){
//     if (!req.timedout) next();
    
//   }

//

// const callapi = app.get('/api/testuser',(req,res)=>{
  

// async function makeConnection() {
//   try {
//     connection = await oracledb.getConnection({
//         user: process.env.oracleUser,
//         password: password,
//         connectString: process.env.localhost
//     });
//    // console.log('connected to database');
//   } catch (err) {
//    if(err !== null ){console.error(err.message);}
//   } finally {
//     if (connection) {
//       // try {
//       //   // Always close connections
//       //   awaitreturn 
//       //   console.log('close connection success');
//       // } catch (err) {
//       //  if(err !== null ){console.error(err.message);}
//       // }

//       connection.execute(
//         `SELECT *
//          FROM DC_LOCATION`,
//         [],  
//        function(err, result) {
//           if (err) {
//            if(err !== null ){console.error(err.message);}
//             return;
//           }
//           console.log(result.rows);
//        });
  
//     }
//   }

  
  

// }

// })

//jump host for nice database

// var config = {
//   username:'clarity',
//   password:'clarity',
//   host:'10.54.5.141',
//   port:22,
//   dstHost:'10.54.8.162',
//   dstPort:1521,
//   localHost:'127.0.0.1',
//   localPort: 1521
// };

// var tunnel = require('tunnel-ssh');
// tunnel(config, function (error, server) {

//   if(error){
//     console.log(error.message);
//   }

//   console.log("tunnel connected")
  
// });


//call api dc user table



//api dc_cage
//const jwt = require('jsonwebtoken')

//app.use(express.json())


  // app.post('/login', (req, res) => {
  //   // Authenticate User LDAP Here


  //   //After authentichate success give authorization here
  
  //   const username = req.body.username
  //   const user = { name: username }
  
  //   const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' })
  
  //   res.json({ accessToken: accessToken })
  // })
  const cache = require('memory-cache');


  let memCache = new cache.Cache();
  let cacheMiddleware = (duration) => {
      return (req, res, next) => {
          let key =  '__express__' + req.originalUrl || req.url
          let cacheContent = memCache.get(key);
          if(cacheContent){
              res.send( cacheContent );
              return
          }else{
              res.sendResponse = res.send
              res.send = (body) => {
                  memCache.put(key,body,duration*1000);
                  res.sendResponse(body)
              }
              next()
          }
      }

    }

  // serve up production assets//
//app.use(express.static('build'));
// app.use(app.router);




app.use(express.static(__dirname + '/build'));
// let the react app to handle any unknown routes 
// serve up the index.html if express does'nt recognize the route
app.get('/*', function(req, res, next){ 
  res.setHeader('Last-Modified', (new Date()).toUTCString());
  next(); 
});

require('./itdasApi/ITD_LDAP_CON11')(app);
require('./itdasApi/DC_USER')(app);
require('./itdasApi/ITD_LOV')(app);
require('./itdasApi/ITD_REQUEST_LIST')(app);
require('./itdasApi/ITD_REQ_SEQ')(app);
require('./itdasApi/ITD_DASHBOARD_LOB_STAT_BE')(app);
require('./itdasApi/ITD_DASHBOARD_LOB_STAT_nonBE')(app);
require('./itdasApi/ITD_DASHBOARD_LOB_BE_TYPE')(app);
require('./itdasApi/ITD_DASHBOARD_LOB_BE_MDY_CAT')(app);
require('./itdasApi/ITD_DASHBOARD_LOB_nonBE_TYPE')(app);

//create
require('./itdasApi/create/ITD_REQUEST_CREATE')(app);
require('./itdasApi/create/ITD_REQ_GIT_CREATE')(app);
require('./itdasApi/create/ITD_REQ_REQUESTOR_CREATE')(app);
require('./itdasApi/create/ITD_REQ_STAT_UPD_CREATE')(app);
require('./itdasApi/create/ITD_REQ_SYS_TM_CREATE')(app);
require('./itdasApi/create/ITD_REQ_SYS_VENDOR_CREATE')(app);
//view
require('./itdasApi/ITD_REQ_GIT_VIEW')(app);
require('./itdasApi/ITD_REQ_REQUESTOR_VIEW')(app);
require('./itdasApi/ITD_REQ_STAT_UPD_VIEW')(app);
require('./itdasApi/ITD_REQ_SYS_TM_VIEW')(app);
require('./itdasApi/ITD_REQ_SYS_VENDOR_VIEW')(app);
//delete
require('./itdasApi/delete/ITD_REQUEST_DELETE')(app);
require('./itdasApi/delete/ITD_REQ_GIT_DELETE')(app);
require('./itdasApi/delete/ITD_REQ_REQUESTOR_DELETE')(app);
require('./itdasApi/delete/ITD_REQ_STAT_UPD_DELETE')(app);
require('./itdasApi/delete/ITD_REQ_SYS_TM_DELETE')(app);
require('./itdasApi/delete/ITD_REQ_SYS_VENDOR_DELETE')(app);

const PORT = process.env.PORT || 5004;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));



