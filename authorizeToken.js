
const jwt = require('jsonwebtoken')

module.exports = function authenticateToken(req, res, next) {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
        if (token == null) return res.status(200).send('{"response":"unauthorized"}')
      
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
         
        
          if (err) return  res.status(200).send('{"response":"unauthorized"}')
          req.user = user
          next()
        })
      }
