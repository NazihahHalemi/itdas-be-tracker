FROM oraclelinux:7-slim

RUN  yum -y install oracle-release-el7 oracle-nodejs-release-el7 && \
     yum-config-manager --disable ol7_developer_EPEL && \
     yum -y install oracle-instantclient19.3-basiclite nodejs && \
     yum -y install vim && \
     rm -rf /var/cache/yum

#   for tunneling   
# RUN yum -y install openssh-server


WORKDIR /dcportal
ADD package.json /dcportal/
ADD server.js /dcportal/


RUN npm install

RUN npm install pm2 -g
ENV PM2_PUBLIC_KEY wuk3gv9wuy7hoko
ENV PM2_SECRET_KEY 1pydbw7dt7u4vxo



COPY . .
ADD  text.js /dcportal/node_modules/body-parser/lib/types/


EXPOSE 5004

CMD ["pm2-runtime", "server.js"]

