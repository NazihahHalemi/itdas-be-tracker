const oracledb = require('oracledb');
const Promise = require("bluebird");
const authorizetoken = require('../authorizeToken')


var password = process.env.oraclePwd 
module.exports = function (app) {

    const callapi = app.get('/api/ITD_REQUEST_LIST',authorizetoken,(req,res,next)=>{

        Id = req.query.REQ_ID
       
        if (Id != null) {
        query1=" where REQ_ID='"+Id+"'";
        }else{
        query1="";  
         Id=0;
        }   

        makeConnection(query1,res)           
      })   
  }
        async function makeConnection(query1,res) {
        let connection
        try {
          connection = await oracledb.getConnection({
              user: process.env.oracleUser,
              password: password,
              connectString:process.env.localhost
              //connectString:process.env.nicedb
          });
         // console.log('connected to database');
      
         var query1p = await  connection.execute(  
            `select * from itd_request`+query1+``,
            [],           
            ).catch(function(err) {
              console.log(err.message +" error 1 ITD_REQUEST_LIST");
            //  res.status(200).send({"failed":err.message});
              return
          })

            var query2p = await  connection.execute(
            `select * from itd_request_update c where c.ru_req_id=`+Id+``,
            []).catch(function(err) {
              console.log(err.message +" error 2 ITD_REQUEST_LIST");
            //  res.status(200).send({"failed":err.message});
              return
          })

            var query3p =  await connection.execute(
                `select * from itd_request_requestor b where b.rq_req_id=`+Id+``,
              []).catch(function(err) {
                console.log(err.message +" error 3 ITD_REQUEST_LIST");
              //  res.status(200).send({"failed":err.message});
                return
            }) 

              var query4p = await connection.execute(
                `select * from itd_request_git a  where a.rg_req_id=`+Id+``,
                []).catch(function(err) {
                  console.log(err.message +" error 4 ITD_REQUEST_LIST");
                //  res.status(200).send({"failed":err.message});
                  return
              })
    
              var query5p = await connection.execute(
                `select * from itd_request_system_tm d where d.im_req_id=`+Id+``,
                []).catch(function(err) {
                  console.log(err.message +" error 5 ITD_REQUEST_LIST");
                //  res.status(200).send({"failed":err.message});
                  return
              })
    
              var query6p = await connection.execute(
                `select * from itd_request_system_vendor d where d.iv_req_id=`+Id+``,
                []).catch(function(err) {
                  console.log(err.message +" error 6 ITD_REQUEST_LIST");
                //  res.status(200).send({"failed":err.message});
                  return
              })
    
                Promise.join(query1p, query2p, query3p, query4p,query5p,query6p).spread(function (result, result2, result3, result4,result5,result6){
  
                        var req_update = [];
                        var req_requestor = [];
                        var req_git = [];
                        var req_sys_tm = [];
                        var req_sys_vendor = [];
                      
                        res.status(200).send( { request : result.rows, req_update: result2.rows , 
                            req_requestor: result3.rows, req_git: result4.rows,req_sys_tm: result5.rows, req_sys_vendor: result6.rows});   
        
                      
                        
                       //
                      }).catch(function(err) {
                        console.log(err.message +" error promise");
                      //  res.status(200).send({"failed":err.message});
                      res.status(200).send({"failed":err.message});
                     return
                    }
                    )
                    
        

              
              } catch (err) {

                res.status(200).send({"failed":err.message});
               return
              } finally {
                if (connection) {
                
                  try {
                    // Release the connection back to the connection pool
                    await connection.release();
                   // console.log("closed CONNECTION")
                  } catch (err) {
                    console.error(err);
                  }
                }
              } 
              
          }