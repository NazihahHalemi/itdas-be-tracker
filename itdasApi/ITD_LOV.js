const oracledb = require('oracledb');
const Promise = require("bluebird");
const authorizetoken = require('../authorizeToken')


var password = process.env.oraclePwd 
module.exports = function (app) {

    const callapi = app.get('/api/ITD_LOV',(req,res,next)=>{

        type = req.query.type
        value = req.query.value
      
        if (type != null && value != null) {
            query1=" JOIN ITD_LOV B ON A.LOV_ID=B.LOV_PARENT_ID AND A.LOV_NAME='"+type+"' and A.LOV_VALUE='"+value+"'";
            }
            else if (type != null && value == null) {
            query1=" where LOV_NAME='"+type+"'";
            }
            else{
              query1="";
            };
        // if (type != null) {
        //     query1=" where LOV_NAME='"+type+"'";
        //     }
        //     else{
        //       query1="";
        //     };
        makeConnection(query1,res)           
      })   
  }
        async function makeConnection(query1,res) {
        let connection
        try {
          connection = await oracledb.getConnection({
              user: process.env.oracleUser,
              password: password,
              connectString:process.env.localhost
              //connectString:process.env.nicedb
          });
         // console.log('connected to database');
      
          var query1p = await connection.execute(
                `select * from ITD_LOV A `+ query1,
                [],               
              ).catch(function(err) {
                console.log(err.message +" error 1 ITD_LOV");
              //  res.status(200).send({"failed":err.message});
                return 
            })
    
                Promise.join(query1p).spread(function (result){
                  res.status(200).send(result.rows);
                 
                }).catch(function(err) {
                  console.log(err.message +" error promise");
                //  res.status(200).send({"failed":err.message});
                res.status(200).send({"failed":err.message});
               
               return
              })

              
              } catch (err) {

                res.status(200).send({"failed":err.message});
               return
              } finally {
                if (connection) {
                
                  try {
                    // Release the connection back to the connection pool
                    await connection.release();
                   // console.log("closed CONNECTION")
                  } catch (err) {
                    console.error(err);
                  }
                }
              } 
              
          }