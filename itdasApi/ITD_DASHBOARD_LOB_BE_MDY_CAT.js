const oracledb = require('oracledb');
const Promise = require("bluebird");
//const authorizetoken = require('../authorizeToken')

var password = process.env.oraclePwd 
module.exports = function (app) {

    const callapi = app.get('/api/ITD_DASHBOARD_LOB_BE_MDY_CAT',//authorizetoken,
    (req,res,next)=>{


        makeConnection(res)       
            })
        }

        async function makeConnection(res) {
           
          let connection
          try {
            connection = await oracledb.getConnection({
                user: process.env.oracleUser,
                password: password,
                connectString:process.env.localhost
                //connectString:process.env.localhost
            });
            //console.log('/DC_USER');
  
            var query1p = await  connection.execute(  
              `select a.lob, 
              (select count (*) from ITD_REQUEST where UPPER(REQ_CATEGORY)='BE'
                      and req_lob=a.lob and upper(req_md_category)='SIMPLE') as Simple,
              (select count (*) from ITD_REQUEST where UPPER(REQ_CATEGORY)='BE'
                      and req_lob=a.lob and upper(req_md_category)='MEDIUM') as Medium,
              (select count (*) from ITD_REQUEST where UPPER(REQ_CATEGORY)='BE'
                      and req_lob=a.lob and upper(req_md_category)='COMPLEX') as Complex,
              a.total from 
              (select req_lob as lob, count(*) as Total from ITD_REQUEST
              where req_md_category is not null and  UPPER(REQ_CATEGORY)='BE' 
              group by req_lob) a
              UNION ALL
              select 'TOTAL',SUM(SIMPLE),SUM(MEDIUM),SUM(COMPLEX),SUM(TOTAL) from (
              select a.lob, 
              (select count (*) from ITD_REQUEST where UPPER(REQ_CATEGORY)='BE'
                      and req_lob=a.lob and upper(req_md_category)='SIMPLE') as Simple,
              (select count (*) from ITD_REQUEST where UPPER(REQ_CATEGORY)='BE'
                      and req_lob=a.lob and upper(req_md_category)='MEDIUM') as Medium,
              (select count (*) from ITD_REQUEST where UPPER(REQ_CATEGORY)='BE'
                      and req_lob=a.lob and upper(req_md_category)='COMPLEX') as Complex,
              a.total from 
              (select req_lob as lob, count(*) as Total from ITD_REQUEST
              where req_md_category is not null and  UPPER(REQ_CATEGORY)='BE' 
              group by req_lob) a)`,
              [],           
              ).catch(function(err) {
                console.log(err.message +" error 1 ITDAS request git");
              //  res.status(200).send({"failed":err.message});
                return
            })
  
              Promise.join(query1p).spread(function (result){
  
                res.status(200).send( { data: result.rows});   

              
                
               //
              }).catch(function(err) {
                console.log(err.message +" error promise");
              //  res.status(200).send({"failed":err.message});
              res.status(200).send({"failed":err.message});
             return
            }
            )
      
           //
              
          } catch (err) {
            console.error(err.message + " error all ITDAS request git");
            res.status(200).send({"failed":err.message});
           return
          }finally {
            if (connection) {
            
              try {
                // Release the connection back to the connection pool
                await connection.release();
                
              } catch (err) {
                console.error(err);
              }
            }
          }         
      }