const oracledb = require('oracledb');
const Promise = require("bluebird");
//const authorizetoken = require('../authorizeToken')

var password = process.env.oraclePwd 
module.exports = function (app) {

    const callapi = app.get('/api/ITD_DASHBOARD_LOB_STAT_nonBE',//authorizetoken,
    (req,res,next)=>{


        makeConnection(res)       
            })
        }

        async function makeConnection(res) {
           
          let connection
          try {
            connection = await oracledb.getConnection({
                user: process.env.oracleUser,
                password: password,
                connectString:process.env.localhost
                //connectString:process.env.localhost
            });
            //console.log('/DC_USER');
  
            var query1p = await  connection.execute(  
              `select t.req_lob as LOB,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='OPEN') as OPEN,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='REQUIREMENT') as REQUIREMENT,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='ASSESSMENT') as ASSESSMENT,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='APPROVAL') as APPROVAL,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='DESIGN') as DESIGN,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='BUILD') as BUILD,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='TESTING') as TESTING,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='DEPLOYED') as DEPLOYED,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='COMPLETED') as COMPLETED,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='CLOSED') as CLOSED,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='KIV') as KIV,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='CANCEL') as CANCEL,
              t.total
              from 
              (select req_lob, count(*) total from itd_request 
              WHERE UPPER(REQ_CATEGORY)='NON_BE'
              and req_lob IS NOT NULL 
              and upper(req_status) in ('OPEN','REQUIREMENT','ASSESSMENT','APPROVAL','DESIGN',
              'BUILD','TESTING','DEPLOYED','COMPLETED','CLOSED','KIV','CANCEL')
              group by req_lob) t
              UNION ALL
              SELECT 'TOTAL', 
              SUM(OPEN),
              SUM(REQUIREMENT),
              SUM(ASSESSMENT),
              SUM(APPROVAL),
              SUM(DESIGN),
              SUM(BUILD),
              SUM(TESTING),
              SUM(DEPLOYED),
              SUM(COMPLETED),
              SUM(CLOSED),
              SUM(KIV),
              SUM(CANCEL),
              SUM(TOTAL)
              FROM
              (
              select t.req_lob as LOB,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='OPEN') as OPEN,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='REQUIREMENT') as REQUIREMENT,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='ASSESSMENT') as ASSESSMENT,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='APPROVAL') as APPROVAL,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='DESIGN') as DESIGN,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='BUILD') as BUILD,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='TESTING') as TESTING,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='DEPLOYED') as DEPLOYED,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='COMPLETED') as COMPLETED,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='CLOSED') as CLOSED,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='KIV') as KIV,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and upper(req_status)='CANCEL') as CANCEL,
              t.total
              from 
              (select req_lob, count(*) total from itd_request 
              WHERE UPPER(REQ_CATEGORY)='NON_BE'
              and req_lob IS NOT NULL 
              and upper(req_status) in ('OPEN','REQUIREMENT','ASSESSMENT','APPROVAL','DESIGN',
              'BUILD','TESTING','DEPLOYED','COMPLETED','CLOSED','KIV','CANCEL')
              group by req_lob) t
              )
              
              --SELECT * FROM ITD_LOV WHERE LOV_NAME='STATUS'`,
              [],           
              ).catch(function(err) {
                console.log(err.message +" error 1 ITDAS request git");
              //  res.status(200).send({"failed":err.message});
                return
            })
  
              Promise.join(query1p).spread(function (result){
  
                res.status(200).send( { data: result.rows});   

              
                
               //
              }).catch(function(err) {
                console.log(err.message +" error promise");
              //  res.status(200).send({"failed":err.message});
              res.status(200).send({"failed":err.message});
             return
            }
            )
      
           //
              
          } catch (err) {
            console.error(err.message + " error all ITDAS request git");
            res.status(200).send({"failed":err.message});
           return
          }finally {
            if (connection) {
            
              try {
                // Release the connection back to the connection pool
                await connection.release();
                
              } catch (err) {
                console.error(err);
              }
            }
          }         
      }