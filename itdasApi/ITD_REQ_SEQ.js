const oracledb = require('oracledb');
const Promise = require("bluebird");
const authorizetoken = require('../authorizeToken')


var password = process.env.oraclePwd 
module.exports = function (app) {

    const callapi = app.get('/api/ITD_REQ_SEQ',(req,res,next)=>{

         query1="select ITD_REQ_ID_SEQ.NEXTVAL as req_id from dual";
        
        makeConnection(query1,res)           
      })   
  }
        async function makeConnection(query1,res) {
        let connection
        try {
          connection = await oracledb.getConnection({
              user: process.env.oracleUser,
              password: password,
              connectString:process.env.localhost
              //connectString:process.env.nicedb
          });
         // console.log('connected to database');
      
          var query1p = await connection.execute(
               query1,
                [],               
              ).catch(function(err) {
                console.log(err.message +" error 1 ITD_REQ_SEQ");
              //  res.status(200).send({"failed":err.message});
                return 
            })
    
                Promise.join(query1p).spread(function (result){
                  res.status(200).send(result.rows);
                 
                }).catch(function(err) {
                  console.log(err.message +" error promise");
                //  res.status(200).send({"failed":err.message});
                res.status(200).send({"failed":err.message});
               
               return
              })

              
              } catch (err) {

                res.status(200).send({"failed":err.message});
               return
              } finally {
                if (connection) {
                
                  try {
                    // Release the connection back to the connection pool
                    await connection.release();
                   // console.log("closed CONNECTION")
                  } catch (err) {
                    console.error(err);
                  }
                }
              } 
              
          }