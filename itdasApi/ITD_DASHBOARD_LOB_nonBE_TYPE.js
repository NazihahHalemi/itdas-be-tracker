const oracledb = require('oracledb');
const Promise = require("bluebird");
//const authorizetoken = require('../authorizeToken')

var password = process.env.oraclePwd 
module.exports = function (app) {

    const callapi = app.get('/api/ITD_DASHBOARD_LOB_nonBE_TYPE',//authorizetoken,
    (req,res,next)=>{


        makeConnection(res)       
            })
        }

        async function makeConnection(res) {
           
          let connection
          try {
            connection = await oracledb.getConnection({
                user: process.env.oracleUser,
                password: password,
                connectString:process.env.localhost
                //connectString:process.env.localhost
            });
            //console.log('/DC_USER');
  
            var query1p = await  connection.execute(  
              `select t.req_lob as LOB,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Initiative') as Initiative,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Project') as Project,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Consultancy') as Consultancy,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Business Support') as Business_Support,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Event/Meeting') as Event_Meeting,
              t.total
              from 
              (select req_lob, count(*) total from itd_request 
               WHERE UPPER(REQ_CATEGORY)='NON_BE'
              and req_lob IS NOT NULL 
              and req_type in ('Initiative','Project','Consultancy','Business Support','Event/Meeting') group by req_lob) t              
              union all
              SELECT 'TOTAL',
              SUM(Initiative),
              SUM(Project),
              SUM(Consultancy),
              SUM(Business_Support),
              SUM(Event_Meeting),
              SUM(total)
               FROM 
              (select req_lob as LOB,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Initiative') as Initiative,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Project') as Project,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Consultancy') as Consultancy,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Business Support') as Business_Support,
              (select count(*) from itd_request where UPPER(REQ_CATEGORY)='NON_BE' and req_lob=t.req_lob and req_type='Event/Meeting') as Event_Meeting,
              t.total
              from 
              (select req_lob, count(*) total from itd_request 
               WHERE UPPER(REQ_CATEGORY)='NON_BE'
              and req_lob IS NOT NULL 
              and req_type in ('Initiative','Project','Consultancy','Business Support','Event/Meeting') group by req_lob) t)    `,
              [],           
              ).catch(function(err) {
                console.log(err.message +" error 1 ITDAS request git");
              //  res.status(200).send({"failed":err.message});
                return
            })
  
              Promise.join(query1p).spread(function (result){
  
                res.status(200).send( { data: result.rows});   

              
                
               //
              }).catch(function(err) {
                console.log(err.message +" error promise");
              //  res.status(200).send({"failed":err.message});
              res.status(200).send({"failed":err.message});
             return
            }
            )
      
           //
              
          } catch (err) {
            console.error(err.message + " error all ITDAS request git");
            res.status(200).send({"failed":err.message});
           return
          }finally {
            if (connection) {
            
              try {
                // Release the connection back to the connection pool
                await connection.release();
                
              } catch (err) {
                console.error(err);
              }
            }
          }         
      }