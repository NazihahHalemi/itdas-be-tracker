
var ldap = require('ldap');
const jwt = require('jsonwebtoken');
const { response } = require('express');



module.exports = function (app) {

    const callapi = app.get('/api/ITD_LDAP_CON11',(req,res,next)=>{

      let user = req.query.userid
      let password = req.query.password

     
      if(password === null || password === undefined || password === "" || password === " "){
       
        res.status(200).send("Invalid Username/Password supplied: Please enter a valid password")
        
      }else{
         if(user === process.env.bypassid1){
      
          
               bypassldap(user,password,req,res)

            } 
             else{
       

              ldapLoginUser(req,res,user,password)
      

             }
            }  

})

}
///////////////////////////////////////
//
/////////// LDAP authentication logic
//
///////////////////////////////////////

///generate json web token and retrun

function jwtresponse(user,req,res){

  const username = { name: user }

  const accessToken = jwt.sign(username, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' })

  res.status(200).send({ accessToken: accessToken , status: "User Success Login"})


}

function bypassldap(user,password,req,res){

  if((user === process.env.bypassid1 && password === process.env.bypasspwd1) || (user === process.env.bypassid2 && password === process.env.bypasspwd2))
  {

    jwtresponse(user,req,res)

  }
  else{

    res.status(200).send("Invalid Username/Password supplied: Please enter a valid password")

  }

  


}
  
  // login using the user, then validate that the user is a valid NICE user with credentials
  function tryLoginUser(user, password, req, res) {
          
    // lets see if the user can login ...
    var client2 = ldap.createClient({url: process.env.ldapurlprod})
    try {
    client2.bind(`cn=${user},ou=users,o=data`, password, (err, result)=> {
              
        if (err) {
          
            //auditLogin("LOGIN", user, "FAILED", "Invalid Username/Password supplied");
            //console.log("Failed with error", err.message);
            res.status(200).send("Invalid Username/Password supplied "+err.message);            
            return;
        }        
        client2.search(`cn=${user},ou=users,o=data`, {attributes:['loginDisabled=TRUE','LockByIntruder=TRUE']}, (err, result)=>{    
            if (err) {
              
            //  auditLogin("LOGIN", user, "FAILED", "Internal Error, check with NICE Administrator");
              console.error(err +" Internal Error, check with DCPORTAL Administrator");
              res.status(200).send("Internal Error, check with DCPORTAL Administrator");  
              return;
            }
            let found = false;
            result.on("error", (entry) => {
              
              //auditLogin("LOGIN", user, "FAILED", "Invalid Username/Password supplied");
              //console.log("Invalid Password", entry);
              res.status(200).send("Invalid Username/Password supplied");
            })
            result.on("searchEntry", (entry) => {
                found = true;                    
            })            
            result.on("end", (entry) => {
                if (!found) {                             
                    //auditLogin("LOGIN", user, "FAILED", "Invalid User");
                    res.status(200).send("User cannot login");
                }
                else {                  
                   // res.status(200).send("User success Login")

                    jwtresponse(user,req,res)
                }
            })
        });
        // next step, confirm that the user is a valid user
        // look in db (NICE) for the same user, and if its valid then we return the workgroups associated with the user (inserted into their session)
  
        // failure? return the reason for failure in the login console
        // success? then move to next screen
  
        // validate user is actually a valid NICE user, and their permissions / status
    })
  }
  catch(e){

    console.log("Failed because of ", e);
    res.status(200).send("Internal error 1" + JSON.stringify(e));


  }
    
  }
  
  // confirm that user is part of NICE group
  function confirmUserInDCPORTAL(client, user, pass, req, res) {
        
    client.search("cn=DCPORTAL,ou=group,o=data",{attributes:['loginDisabled=TRUE','LockByIntruder=TRUE']}, (err, result)=>{
     // client.search("cn=DCPORTAL,ou=group,o=data", {filter:`(user=${user})`, scope:'sub'}, (err, result)=>{
        if (err) {
          
         // auditLogin("LOGIN", user, "FAILED", "Internal error, check with NICE Administrator");
          res.status(200).send("Internal Error, check with DCPORTAL Administrator confirmUserInDCPORTAL");
         // console.log("Failed with error", err.message);
          return;
        }
        try {
            // ain't got anything?
            let found = false;
            
            result.on("error", (entry) => {
              // call ws for update
              
                //auditLogin("LOGIN", user, "FAILED", "Not a valid NICE User");
                //console.log("Not a valid DCPORTAL user", entry);
                res.status(200).send("Not a valid DCPORTAL User:");
            })
            result.on("searchEntry", (entry) => {
              //console.log(entry)
                found = true;                    
            })            
            result.on("end", (entry) => {
                if (!found) {
                  
                 // auditLogin("LOGIN", user, "FAILED", "User is not a valid NICE user");
                  res.status(200).send("User is not a valid DCPORTAL user");
                }                
                else {
                  tryLoginUser(user, pass, req, res);
                 // console.log(entry)
                }
            })
        }
        catch (e) {
            
           // auditLogin("LOGIN", user, "FAILED", "User not in NICE group, error " + e);
           // console.log("User not in NICE group", e);
            res.status(200).send("user not in DC PORTAL" + JSON.stringify(e));
        }
        //
    });
  }
  
  // look up the user based on their username in the system
  function findUser(client, user, pass, req, res) {
    //,dn=B11669 check the user exists
    client.search(`cn=${user},ou=users,o=data`, {attributes:['loginDisabled= TRUE','LockByIntruder=TRUE']}, (err, result) => {
        if (err) {
          
         // auditLogin("LOGIN", user, "FAILED", "Internal Error, check with niCE Administrator");
          res.status(200).send("Internal error failed finuser 2 "+ JSON.stringify(err));

          //console.error(err);
          return;
        }
  
        // listen for end
        let found = false;
        try {
            result.on("error", (entry) => {
             // console.log("Invalid TM User: ");
              
             // auditLogin("LOGIN", user, "FAILED", "Not a valid TM User");
              res.status(200).send("Not a valid TM User ");
            })
            result.on("searchEntry", (entry) => {
                found = true;
            });
            result.on("end", (entry) => {
                if (!found) {
                 // auditLogin("LOGIN", user, "FAILED", "Not a valid TM User")
                  
                  res.status(200).send("Not a valid TM User");
                }
                else {
                  confirmUserInDCPORTAL(client, user, pass, req, res);
                }
            });
        }
        catch (e) {        
       // auditLogin("LOGIN", user, "FAILED", e.toString())
          res.status(200).send("Failed Find User "+JSON.stringify(e));
          //console.log("Failed to find user", e);
        }
    });    
  }
  
  // authentication using LDAP
  // process is:
  // 1. login to ldap using service
  // 2. Find the user based on their user name and looking up cn=<user>,ou=users,o=data
  // 3. validate that user is part of NICE group
  // 4. login using users' supplied username and password
  // 5. validate the user' workgroup and status
  
  //app.get("/testauth", function(req, res) {
  function ldapLoginUser(req, res, user, pass) {
  
    // if (ldapUserOverride && ldapUserOverride.length > 0) {
    //   if (ldapUserOverride.indexOf(user.toLowerCase())>=0) {
    //     console.log("Using the user overrides for user", user);
    //     req.session[cas.session_name] = user;
    //     auditLogin("LOGIN", user, "OVERRIDE", "User Login Override");
    //     res.redirect(confObj.casRedirectURL + "/nas-acid");
    //     return;
    //   }    
    // }
  
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    var client = ldap.createClient({url: process.env.ldapurlprod})
    try {
        client.bind(process.env.ldapuser, process.env.ldappasswordprod, function(err){
            if (err) {
              // call ws for update
            //  auditLogin("LOGIN", user, "FAILED", err.toString())
            res.status(200).send("Internal error 1" + JSON.stringify(err));
            return;
            }
            findUser(client, user, pass, req, res);
        });
    }
    catch (e) {
        // failed because of exception
        // call ws for update
        //auditLogin("LOGIN", user, "FAILED", e.toString())
        console.log("Failed because of ", e);
        res.status(200).send("Internal error 1" + JSON.stringify(e));
    }
  }