const oracledb = require('oracledb');
const Promise = require("bluebird");
const bodyParser = require('body-parser')
const authorizetoken = require('../../authorizeToken')


var password = process.env.oraclePwd 
module.exports = function (app) {
   
  // parse application/x-www-form-urlencoded

  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.text({ type: 'application/json' }))
    const callapi = app.post('/api/ITD_REQUEST_CREATE',authorizetoken,(req,res)=>{
     
      result1 = JSON.parse(req.body)
      result = result1.data
      // console.log(result)
     
     makeConnection(res,req)           
    })   
}  
      async function makeConnection(res,req) {
        try {
          connection = await oracledb.getConnection({
              user: process.env.oracleUser,
              password: password,
              connectString:process.env.localhost
              //connectString:process.env.nicedb
          });
         // console.log('connected to database');

            var query1p = await connection.execute(
                `declare
                err_msg varchar2(1000);
                begin
                  itdas_tracker.request_insert(ID=>'`+result['REQ_ID']+`',
                                 REF_NO=>'`+result['REQ_REF_NO']+`',
                                EXT_NO=>'`+result['REQ_EXT_NO']+`',
                                CATEGORY=>'`+result['REQ_CATEGORY']+`',
                                TYPE=>'`+result['REQ_TYPE']+`',
                                AGILE=>'`+result['REQ_AGILE']+`',
                                AOP=>'`+result['REQ_AOP']+`',
                                ANA_PLAN_REF_NO=>'`+result['REQ_ANA_PLAN_REF_NO']+`',
                                PLAN_PBE_NO=>'`+result['REQ_PLAN_PBE_NO']+`',
                                PLAN_PBE_DATE=>'`+result['REQ_PLAN_PBE_DATE']+`',
                                ACTUAL_PBE_NO=>'`+result['REQ_ACTUAL_PBE_NO']+`',
                                ACTUAL_PBE_DATE=>'`+result['REQ_ACTUAL_PBE_DATE']+`',
                                QTR_PLAN_RFS=>'`+result['REQ_QTR_PLAN_RFS']+`',
                                DESCR=>'`+result['REQ_DESC']+`',
                                SYSTEM=>'`+result['REQ_SYSTEM']+`',
                                REMARKS=>'`+result['REQ_REMARKS']+`',
                                SRPI_NAME=>'`+result['REQ_SRPI_NAME']+`',
                                STATUS=>'`+result['REQ_STATUS']+`',
                                STATUS_DESC=>'`+result['REQ_STATUS_DESC']+`',
                                CONSULT_1=>'`+result['REQ_CONSULT_1']+`',
                                TAGCOST_1=>'`+result['REQ_TAGCOST_1']+`',
                                CONSULT_2=>'`+result['REQ_CONSULT_2']+`',
                                TAGCOST_2=>'`+result['REQ_TAGCOST_2']+`',
                                CONSULT_3=>'`+result['REQ_CONSULT_3']+`',
                                TAGCOST_3=>'`+result['REQ_TAGCOST_3']+`',
                                PIP_PILLAR=>'`+result['REQ_PIP_PILLAR']+`',
                                PIP_PILLAR_REVENUE=>'`+result['REQ_PIP_PILLAR_REVENUE']+`',
                                MD_CATEGORY=>'`+result['REQ_MD_CATEGORY']+`',
                                BALLPARK_TM=>'`+result['REQ_BALLPARK_TM']+`',
                                BALLPARK_VENDOR=>'`+result['REQ_BALLPARK_VENDOR']+`',
                                TOTAL_TAGCOST_TM=>'`+result['REQ_TOTAL_TAGCOST_TM']+`',
                                BUILD_TM=>'`+result['REQ_BUILD_TM']+`',
                                TEST_TM=>'`+result['REQ_TEST_TM']+`',
                                TOTAL_IA_TM=>'`+result['REQ_TOTAL_IA_TM']+`',
                                TOTAL_TAGCOST_EXT=>'`+result['REQ_TOTAL_TAGCOST_EXT']+`',
                                BUILD_EXT=>'`+result['REQ_BUILD_EXT']+`',
                                TEST_EXT=>'`+result['REQ_TEST_EXT']+`',
                                TOTAL_IA_EXT=>'`+result['REQ_TOTAL_IA_EXT']+`',
                                BUDGET_MEMO_DATE=>'`+result['REQ_BUDGET_MEMO_DATE']+`',
                                BUDGET_MEMO_AMT=>'`+result['REQ_BUDGET_MEMO_AMT']+`',
                                BUDGET_TRANSFER_DATE=>'`+result['REQ_BUDGET_TRANSFER_DATE']+`',
                                BUDGET_TRANSFER_MDY=>'`+result['REQ_BUDGET_TRANSFER_MDY']+`',
                                BUDGET_TRANSFER_AMT=>'`+result['REQ_BUDGET_TRANSFER_AMT']+`',
                                MOT_ADVISE=>'`+result['REQ_MOT_ADVISE']+`',
                                MOT_DATE_SEND=>'`+result['REQ_MOT_DATE_SEND']+`',
                                MOT_RECEIVED=>'`+result['REQ_MOT_RECEIVED']+`',
                                MOT_SEND_USER=>'`+result['REQ_MOT_SEND_USER']+`',
                                MOT_REMARK=>'`+result['REQ_MOT_REMARK']+`',
                                REQUEST_DATE=>'`+result['REQ_REQUEST_DATE']+`',
                                BE_RECEIVED=>'`+result['REQ_BE_RECEIVED']+`',
                                ITDC_BALLPARK=>'`+result['REQ_ITDC_BALLPARK']+`',
                                ITDC_APPROVE_FINAL=>'`+result['REQ_ITDC_APPROVE_FINAL']+`',
                                IBER_BALLPARK=>'`+result['REQ_IBER_BALLPARK']+`',
                                IBER_APPROVE_FINAL=>'`+result['REQ_IBER_APPROVE_FINAL']+`',
                                PCM1_PSC_APPROVE=>'`+result['REQ_PCM1_PSC_APPROVE']+`',
                                PCM2_PSC_APPROVE=>'`+result['REQ_PCM2_PSC_APPROVE']+`',
                                INSERT_BY=>'`+result['REQ_INSERT_BY']+`',
                                               err_message => err_msg);
                end;
                `,
                [],                
              ).catch(function(err) {
                console.log(err.message +" error 1 ITD_REQUEST_CREATE");
              //  res.status(200).send({"failed":err.message});
                return 
            })

              Promise.join(query1p).spread(function (){           
                res.status(200).send("success");
                //console.log(req.body)
                }).catch(function(err) {
                  console.log(err.message +" error promise");
                //  res.status(200).send({"failed":err.message});
                res.status(200).send({"failed":err.message});

               return
              })
              } catch (err) {
             
                res.status(200).send({"failed":err.message});

               return
              } finally {
                if (connection) {
                
                  try {
                    // Release the connection back to the connection pool
                    await connection.release();
                   // console.log("closed CONNECTION")
                  } catch (err) {
                    console.error(err);
                  }
                }
              } 
              
          }