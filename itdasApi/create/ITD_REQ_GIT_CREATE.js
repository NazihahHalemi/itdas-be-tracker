const oracledb = require('oracledb');
const Promise = require("bluebird");
const bodyParser = require('body-parser')
const authorizetoken = require('../../authorizeToken')


var password = process.env.oraclePwd 
module.exports = function (app) {
   
  // parse application/x-www-form-urlencoded

  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.text({ type: 'application/json' }))
    const callapi = app.post('/api/ITD_REQ_GIT_CREATE',authorizetoken,(req,res)=>{
     
      result1 = JSON.parse(req.body)
      result = result1.data
      // console.log(result)
     
     makeConnection(res,req)           
    })   
}  
      async function makeConnection(res,req) {
        try {
          connection = await oracledb.getConnection({
              user: process.env.oracleUser,
              password: password,
              connectString:process.env.localhost
              //connectString:process.env.nicedb
          });
         // console.log('connected to database');

            var query1p = await connection.execute(
                `
                begin
                INSERT INTO ITD_REQUEST_GIT(RG_REQ_ID,RG_EMAIL,RG_NAME,RG_SYSTEM,RG_TAGCOST)values (
                  '`+result['RG_REQ_ID']+`',
                  '`+result['RG_EMAIL']+`',
                  '`+result['RG_NAME']+`',
                  '`+result['RG_SYSTEM']+`',
                  '`+result['RG_TAGCOST']+`'); 
                COMMIT;
                end;`,
                [],                
              ).catch(function(err) {
                console.log(err.message +" error 1 ITD_REQ_GIT_CREATE");
              //  res.status(200).send({"failed":err.message});
                return 
            })

              Promise.join(query1p).spread(function (){           
                res.status(200).send("success");
                //console.log(req.body)
                }).catch(function(err) {
                  console.log(err.message +" error promise");
                //  res.status(200).send({"failed":err.message});
                res.status(200).send({"failed":err.message});

               return
              })
              } catch (err) {
             
                res.status(200).send({"failed":err.message});

               return
              } finally {
                if (connection) {
                
                  try {
                    // Release the connection back to the connection pool
                    await connection.release();
                   // console.log("closed CONNECTION")
                  } catch (err) {
                    console.error(err);
                  }
                }
              } 
              
          }